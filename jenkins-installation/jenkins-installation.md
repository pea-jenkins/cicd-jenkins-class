# 1.Start Jenkins server with docker
```
docker run -d \
  -p 8080:8080 -p 50000:50000 \
  --name jenkins \
  --volume $PWD/jenkins_host/:/var/jenkins_home/ \
  jenkins/jenkins:lts
```

# 2.open browser, http://localhost:8080
```
docker exec jenkins tail /var/jenkins_home/secrets/initialAdminPassword
```