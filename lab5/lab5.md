# Create folder named “Jenkins” in the superuser’s home folder. And also prepare jenkins_home folder inside jenkins folder. 
```
mkdir jenkins
cd jenkins
mkdir jenkins_home
```

# build the Docker image
```
docker build -t jenkins-with-docker .
```

# Run jenkins container
```
docker compose up -d 
or docker-compose up -d
```

# Get admin key
```
docker exec jenkins_2 tail /var/jenkins_home/secrets/initialAdminPassword
```
